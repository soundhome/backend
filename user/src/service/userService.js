const User = require('../models/user')

exports.saveService = async (body) => {
  return User.create(body)
}

exports.updateService = async (id, body) => {
  const query = await User.updateOne({ id }, { body })
    .then(resp => !!resp)
    .catch(err => {
      console.log(Error(err).message)
      return undefined
    })
  return query
}

exports.findByIdService = async (id) => {
  const query = await User.findById(id)
    .then(resp => resp)
    .catch(err => {
      console.log(Error(err).message)
      return undefined
    })
  return query
}

exports.findByEmailService = async email => {
  const query = await User.findOne({ email })
    .then(resp => resp)
    .catch(err => {
      console.log(Error(err).message)
      return undefined
    })
  return query
}

exports.deleteService = async (id) => {
  const query = await User.deleteOne({ id }).then((resp) => resp).catch(err => {
    console.log(Error(err).message)
    return false
  })
  return query
}
