const express = require('express')
const { auth } = require('../middlewares/auth')
const { saveController, updateController, findByIdController, findByEmailController, deleteController } = require('../controllers/userController')

const router = express.Router()
router.use(auth)

router.get('', async (req, res) => {
  const id = req.userId
  const { status, body } = await findByIdController(id)
  return res.status(status).send(body)
})

router.get('/email/:email', async (req, res) => {
  const email = req.params.email
  const { status, body } = await findByEmailController(email)
  return res.status(status).send(body)
})

router.post('/register', async (req, res) => {
  const { status, body } = await saveController(req.body)
  return res.status(status).send(body)
})

router.put('', async (req, res) => {
  const id = req.userId
  const { status, body } = await updateController(id, req.body)
  return res.status(status).send(body)
})

router.delete('', async (req, res) => {
  const id = req.userId
  const { status, body } = await deleteController(id)
  return res.status(status).send(body)
})

module.exports = app => app.use('/user', router)
