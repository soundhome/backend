const express = require('express')
const router = express.Router()
const { login } = require('../controllers/authController')

router.post('/login', async (req, res) => {
  const { email, password } = req.body
  const { status, body } = await login(email, password)
  res.status(status).send(body)
})

module.exports = app => app.use('/auth', router)
