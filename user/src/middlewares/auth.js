const { decodToken } = require('../utils/jwt')
const { STATUS_CODE, ERROR } = require('../utils/commns')

exports.auth = (req, res, next) => {
  if (req.url !== '/register') {
    const authHeader = req.headers ? req.headers.authorization : undefined
    const token = getToken(authHeader)
    const decoded = decodToken(token)
    console.log(decoded)
    if (!decoded) {
      return res.status(STATUS_CODE.UNAUTHORIZED).send(ERROR.UNAUTHORIZED)
    }
    req.userId = decoded.id
  }

  return next()
}

const getToken = authHeader => {
  if (!authHeader) {
    return undefined
  }

  const parts = authHeader.split(' ')

  if (!parts.length === 2) {
    return undefined
  }

  const [schema, token] = parts

  if (!schema === 'Brearer') {
    return undefined
  }

  return token
}
