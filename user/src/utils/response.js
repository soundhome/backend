exports.mountResponse = (status, body) => {
  if (body.user) {
    body.user.password = undefined
  }

  if (body.password) {
    body.password = undefined
  }

  return { status, body }
}
