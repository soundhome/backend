const jwt = require('jsonwebtoken')
require('dotenv').config()

exports.createToken = (userId) => {
  const secret = process.env.SECRET
  return jwt.sign({ id: userId }, secret, { expiresIn: 86400 })
}

exports.decodToken = (token) => {
  const secret = process.env.SECRET
  const decoded = jwt.verify(token, secret, (err, decoded) => {
    return !err ? decoded : undefined
  })
  return decoded
}
