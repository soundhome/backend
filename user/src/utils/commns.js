exports.STATUS_CODE = {
  OK: 200,
  CREATED: 201,
  NOT_FOUND: 404,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  INTERNAL_SERVER_ERROR: 500
}

exports.ERROR = {
  REGISTERING: 'error to registering',
  USER_NOT_FOUND: 'user not found',
  UNAUTHORIZED: 'unauthorized user'
}
