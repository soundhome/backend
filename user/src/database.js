const mongoose = require('mongoose')
require('dotenv').config()

const uri = process.env.DATA_BASE

mongoose.connect(uri, { useNewUrlParser: true })
mongoose.Promise = global.Promise

module.exports = mongoose
