const express = require('express')

const app = express()

app.use(express.json())

require('./routes/userRoute')(app)
require('./routes/authRoute')(app)

app.listen(3000, () => {
  console.log('servidor ativo na porta 3000')
})
