const {
  saveService,
  updateService,
  findByEmailService,
  findByIdService,
  deleteService
} = require('../service/userService')
const { mountResponse } = require('../utils/response')
const { STATUS_CODE, ERROR } = require('../utils/commns')

const verifyFields = (body) => {
  const { name, email, password } = body
  return name && email && password
}

const verifyRegister = async (body) => {
  console.log(!await findByEmailService(body.email))
  return verifyFields(body) && !await findByEmailService(body.email)
}

exports.saveController = async (body) => {
  if (await verifyRegister(body)) {
    const query = await saveService(body)
    return mountResponse(STATUS_CODE.CREATED, query)
  } else {
    return mountResponse(STATUS_CODE.BAD_REQUEST, { error: ERROR.REGISTERING })
  }
}

exports.updateController = async (id, body) => {
  if (verifyFields(body) && await updateService(id, body)) {
    const user = await findByIdService(id)
    return mountResponse(STATUS_CODE.OK, user)
  } else {
    return mountResponse(STATUS_CODE.BAD_REQUEST, ERROR.BAD_REQUEST)
  }
}

exports.findByIdController = async (id) => {
  const service = await findByIdService(id)
  return service ? mountResponse(STATUS_CODE.OK, service) : mountResponse(STATUS_CODE.BAD_REQUEST, ERROR.USER_NOT_FOUND)
}

exports.findByEmailController = async (email) => {
  const service = await findByEmailService(email)
  return service ? mountResponse(STATUS_CODE.OK, service) : mountResponse(STATUS_CODE.BAD_REQUEST, ERROR.USER_NOT_FOUND)
}

exports.deleteController = async (id) => {
  const service = await deleteService(id)
  console.log(service)
  return service ? mountResponse(STATUS_CODE.OK, service) : mountResponse(STATUS_CODE.BAD_REQUEST, ERROR.USER_NOT_FOUND)
}
