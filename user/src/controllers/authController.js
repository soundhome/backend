const bcrypt = require('bcryptjs')
const { findByEmailService } = require('../service/userService')
const { STATUS_CODE, ERROR } = require('../utils/commns')
const { mountResponse } = require('../utils/response')
const { createToken } = require('../utils/jwt')

const comparePassword = async (password, user) => {
  return !!await bcrypt.compare(password, user.password)
}

exports.login = async (email, password) => {
  const user = await findByEmailService(email)

  if (!user || !await comparePassword(password, user)) {
    return mountResponse(STATUS_CODE.UNAUTHORIZED, ERROR.UNAUTHORIZED)
  }

  const token = createToken(user.id)
  return mountResponse(STATUS_CODE.OK, { user, token })
}
